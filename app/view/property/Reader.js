Ext.define('GPG.view.property.Reader', {
    extend: 'Ext.grid.property.Reader',

    readRecords: function(dataObject) {
        var Model = this.getModel(),
            result = {
                records: [],
                success: true
            };

        for (var i = 0, l = dataObject.length; i < l; i++) {
            result.records.push(new Model(dataObject[i]));
        }

        result.total = result.count = result.records.length;

        return new Ext.data.ResultSet(result);
    }
});
