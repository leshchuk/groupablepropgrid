Ext.define("GPG.view.property.GroupableGrid", {
    extend: "Ext.grid.property.Grid",

    requires: [
        "Ext.grid.feature.Grouping",
        "Ext.util.Grouper"
    ],

    /**
     * @method initComponent
     * @inheritdoc
     * @return {void}
     */
    initComponent: function() {
        var me = this;

        // me.features = me.features || [];
        // me.features.push({
        //     ftype: "grouping",
        //     groupHeaderTpl: [
        //         '{name:this.formatName}',
        //         {
        //             formatName: function(name) {
        //                 return Ext.String.capitalize(name);
        //             }
        //         }
        //     ],
        //     collapsible: !!me.collapsibleGroups
        // });

        me.callParent();

        // me.getStore().setGrouper({
        //     groupFn: me.getRecordGroup.bind(me)
        // })
    },

    // @private
    getRecordGroup: function(rec) {
        var prop = rec.data[this.nameField];
        var sc = this.sourceConfig;
        var gc = this.groupsConfig;
        var group = this.getConfig(prop, "group");

        if (group === undefined) {

            group = "common";

            Ext.Object.each(gc, function(gr, arr) {
                if (arr.indexOf(prop) != -1) {
                    group = gr;
                    return false;
                }
            }, this);

            this.setConfig(prop, "group", group);

        }

        return group;
    },
})
