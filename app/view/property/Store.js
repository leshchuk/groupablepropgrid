Ext.define('GPG.view.property.Store', {

    extend: 'Ext.data.Store',

    alternateClassName: 'GPG.PropertyStore',

    remoteSort: true,

    requires: [
        'Ext.data.proxy.Memory',
        'GPG.view.property.Reader',
        'GPG.view.property.Property'
    ],

    constructor : function(grid, source){
        var me = this;

        me.grid = grid;
        // me.source = source.sort(function(r1, r2) {
        //     var g1 = r1[grid.groupField].toLowerCase(),
        //         g2 = r2[grid.groupField].toLowerCase();
        //         n1 = r1[grid.nameField].toLowerCase();
        //         n2 = r2[grid.nameField].toLowerCase();
        //     if (g1 == g2) return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
        //     if (g1 == "common" && g2 != "common") return -1;
        //     if (g1 != "common" && g2 == "common") return 1;
        //     return g1 < g2 ? -1 : g1 > g2 ? 1 : 0;
        // });
        me._model = GPG.view.property.Property;
        me.callParent([{
            data: source,
            model: me._model,
            proxy: me.getProxy(),
            groupField: grid.groupField,
            groupDir: "DESC",
            sorters: [
                new Ext.util.Sorter({
                    sorterFn: function(r1, r2) {
                        debugger
                        var g1 = r1.data[me.grid.groupField].toLowerCase(),
                            g2 = r2.data[me.grid.groupField].toLowerCase();
                            n1 = r1.data[me.grid.nameField].toLowerCase();
                            n2 = r2.data[me.grid.nameField].toLowerCase();
                        if (g1 == g2) return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                        if (g1 == "common" && g2 != "common") return -1;
                        if (g1 != "common" && g2 == "common") return 1;
                        return g1 < g2 ? -1 : g1 > g2 ? 1 : 0;
                    }
                })
            ]
            // sorters: [
            //     new Ext.util.Sorter({
            //         // Sort by first letter of last name, in descending order
            //         sorterFn: function(r1, r2) {
            //             debugger
            //             var g1 = r1.data[me.groupField].toLowerCase(),
            //                 g2 = r2.data[me.groupField].toLowerCase();
            //             if (g1 == "common" && g2 != "common") return -1;
            //             if (g1 != "common" && g2 == "common") return 1;
            //             return g1 < g2 ? -1 : g1 > g2 ? 1 : 0;
            //         },
            //         direction: 'ASC'
            //     }),
            //     new Ext.util.Sorter({
            //         // Sort by first letter of last name, in descending order
            //         sorterFn: function(r1, r2) {
            //             debugger
            //             var g1 = r1.data[me.nameField].toLowerCase(),
            //                 g2 = r2.data[me.nameField].toLowerCase();
            //             return g1 < g2 ? -1 : g1 > g2 ? 1 : 0;
            //         },
            //         direction: 'ASC'
            //     })
            // ]
        }]);
    },

    // Return a singleton, customized Proxy object which configures itself with a custom Reader
    getProxy: function() {
        var proxy = this.proxy;
        if (!proxy) {
            proxy = this.proxy = new Ext.data.proxy.Memory({
                model: this._model,
                reader: this.getReader()
            });
        }
        return proxy;
    },

    // Return a singleton, customized Reader object which reads Ext.grid.property.Property records from an object.
    getReader: function() {
        var reader = this.reader;
        if (!reader) {
            reader = this.reader = new GPG.view.property.Reader({
                model: this._model,
            });
        }
        return reader;
    },

    // @protected
    // Should only be called by the grid. Use grid.setSource instead.
    setSource : function(dataObject) {
        this.source = dataObject;
        this.suspendEvents();
        this.removeAll();
        this.getProxy().setData(dataObject);
        this.load();
        this.resumeEvents();
        this.fireEvent('datachanged', this);
        this.fireEvent('refresh', this);
    },

    // @private
    getProperty : function(row) {
        return Ext.isNumber(row) ? this.getAt(row) : this.getById(row);
    },

    // @private
    setValue : function(prop, value, create){
        var config = {
            name: prop,
            value: value,
            group: "common"
        };
        if (Ext.isObject(prop)) {
            config = Ext.apply({}, {
                name: prop.name,
                value: prop.value,
                group: prop.group
            }, config);
            create = !!arguments[arguments.length == 3 ? 2 : 1];
        }

        var rec = me.getRec(prop);

        if (rec) {
            rec.set('value', value);
            this.source[prop] = value;
        } else if (create) {
            // only create if specified.
            this.source[prop] = value;
            rec = new me._model(config, config.prop);
            this.add(rec);
        }
    },

    // @private
    remove : function(prop) {
        var rec = this.getRec(prop);
        if (rec) {
            this.callParent([rec]);
            delete this.source[prop];
        }
    },

    // @private
    getRec : function(prop) {
        return this.getById(prop);
    },

    // @protected
    // Should only be called by the grid.  Use grid.getSource instead.
    getSource : function() {
        return this.source;
    },

    onDestroy: function() {
        Ext.destroy(this.reader, this.proxy);
        this.callParent();
    }
});
