Ext.define('GPG.view.property.Property', {
    extend: 'Ext.data.Model',

    alternateClassName: "GPG.Property",

    nameField: "name",
    valueField: "value",
    groupField: "group",

    constructor: function(config) {
        config = config || {};

        this.nameField = config.nameField || this.nameField;
        this.valueField = config.valueField || this.valueField;
        this.groupField = config.groupField || this.groupField;

        this.fields = [{
            name: this.nameField,
            type: 'string'
        }, {
            name: this.valueField
        }, {
            name: this.groupField,
            type: 'string'
        }];

        this.idProperty = this.nameField;

        return this.callParent(arguments);
    }
});
