Ext.define("GPG.view.property.Grid", {
    extend: "Ext.grid.Panel",
    alias: "widget.x-propertygrid",

    requires: [
        "Ext.grid.feature.Grouping",
        "GPG.view.property.Store"
    ],

    inferTypes: true,
    nameColumnWidth: 200,
    nameField: "name",
    valueField: "value",
    groupField: "group",

    config: {
        source: [],
        sourceConfig: {},
        groupsConfig: {}
    },

    enableColumnMove: false,
    columnLines: true,
    stripeRows: false,
    trackMouseOver: false,
    clicksToEdit: 1,
    enableHdMenu: false,

    sortableColumns: false,

    initComponent: function() {
        var me = this;

        me.source = me.source || [];
        me.sourceConfig = me.sourceConfig || {};
        me.groupsConfig = me.groupsConfig || {};

        me.plugins = me.plugins || [];

        // Enable cell editing. Inject a custom startEdit which always edits column 1 regardless of which column was clicked.
        me.plugins.push(new Ext.grid.plugin.CellEditing({
            clicksToEdit: me.clicksToEdit,

            // Inject a startEdit which always edits the value column
            startEdit: function(record, column) {
                // Maintainer: Do not change this 'this' to 'me'! It is the CellEditing object's own scope.
                return this.self.prototype.startEdit.call(this, record, me.valueColumn);
            }
        }));

        me.selModel = {
            selType: 'cellmodel',
            onCellSelect: function(position) {
                // We are only allowed to select the value column.
                position.column = me.valueColumn;
                position.colIdx = me.valueColumn.getVisibleIndex();
                return this.self.prototype.onCellSelect.call(this, position);
            }
        };

        me.features = me.features || [];
        me.features.push({
            ftype: "grouping",
            groupHeaderTpl: [
                '{name:this.formatName}',
                {
                    formatName: function(name) {
                        return Ext.String.capitalize(name);
                    }
                }
            ],
            collapsible: !!me.collapsibleGroups
        });

        me.store = Ext.create('GPG.view.property.Store', me, me.source);

        me.columns = new Ext.grid.property.HeaderContainer(me, me.store);

        me.callParent();

        // Inject a custom implementation of walkCells which only goes up or down
        me.getView().walkCells = me.walkCells;

        me.editors = Ext.apply({}, me.editors, {
            'date'    : new Ext.grid.CellEditor({ field: new Ext.form.field.Date({selectOnFocus: true})}),
            'string'  : new Ext.grid.CellEditor({ field: new Ext.form.field.Text({selectOnFocus: true})}),
            'number'  : new Ext.grid.CellEditor({ field: new Ext.form.field.Number({selectOnFocus: true})}),
            'boolean' : new Ext.grid.CellEditor({ field: new Ext.form.field.ComboBox({
                editable: false,
                store: [[ true, me.headerCt.trueText ], [false, me.headerCt.falseText ]]
            })})
        });

        me.store.on('update', me.onUpdate, me);
    },

    applySource: function(value) {
        var source = [];
        if (Ext.isArray(value)) {
            source = value;
        }
        if (Ext.isObject(value)) {
            Ext.Object.each(value, function(key, val) {
                var data = {};
                data[this.nameField] = key;
                data[this.valueField] = val;
                data[this.groupField] = this.getConfig(key, "group", "common");
                source.push(data);
            }, this);
        }
        // if (this.store) this.store.setData(source);
        return source;
    },

    applySourceConfig: function(value) {
        var sc = Ext.apply({}, value, this.sourceConfig),
            gc = this.groupsConfig,
            src = this.source;
        var name, value, type;
        if (gc) {
            Ext.Object.each(gc, function(key, arr) {
                Ext.Array.from(arr).forEach(function(fld) {
                    sc[fld] = sc[fld] || {};
                    sc[fld].group = sc[fld].group || key.toLowerCase();
                })
            });
        }
        if (src) {
            Ext.Array.from(src).forEach(function(rec) {
                name = rec[this.nameField];
                if (this.inferTypes) {
                    sc[name] = sc[name] || {};
                    if (!sc[name].type) {
                        value = rec[this.valueField];
                        if (Ext.isDate(value)) {
                            type = 'date';
                        } else if (Ext.isNumber(value)) {
                            type = 'number';
                        } else if (Ext.isBoolean(value)) {
                            type = 'boolean';
                        } else {
                            type = 'string';
                        }
                        sc[name].type = type;
                    }
                }
                if (sc[name] && sc[name].group) {
                    rec.group = sc[name].group;
                }
            }, this);
        }
        return sc;
    },

    applyGroupsConfig: function(value) {
        var gc = Ext.apply({}, value),
            sc = this.sourceConfig,
            src = this.source,
            rec;
        // if (!sc) sc = this.sourceConfig = {};
        if (this.sourceConfig) {
            Ext.Object.each(gc, function(key, arr) {
                Ext.Array.from(arr).forEach(function(fld) {
                    this.sourceConfig[fld] = this.sourceConfig[fld] || {};
                    this.sourceConfig[fld].group = this.sourceConfig[fld].group || key.toLowerCase();
                    if (src) {
                        rec = Ext.Array.forEach(src, function(item) {
                            if (item[this.nameField] == fld) {
                                item[this.groupField] = this.sourceConfig[fld].group;
                                return false;
                            }
                        }, this);
                    }
                }, this);
            }, this);
        }
        return gc;
    },

    getConfig: function(fieldName, key, defaultValue) {
        if (!this.sourceConfig) return defaultValue;
        var config = this.sourceConfig[fieldName],
            out;

        if (config) {
            out = config[key];
        }
        return out || defaultValue;
    },

    setConfig: function(fieldName, key, value) {
        if (!this.sourceConfig) return;
        var sourceCfg = this.sourceConfig,
            o = sourceCfg[fieldName];

        if (!o) {
            o = sourceCfg[fieldName] = {
                __copied: true
            };
        } else if (!o.__copied) {
            o = Ext.apply({
                __copied: true
            }, o);
            sourceCfg[fieldName] = o;
        }
        o[key] = value;
        return value;
    },

    // @private
    getCellEditor : function(record, column) {
        var me = this,
            propName = record.get(me.nameField),
            val = record.get(me.valueField),
            editor = me.getConfig(propName, 'editor'),
            type = me.getConfig(propName, 'type'),
            editors = me.editors;

        // A custom editor was found. If not already wrapped with a CellEditor, wrap it, and stash it back
        // If it's not even a Field, just a config object, instantiate it before wrapping it.
        if (editor) {
            if (!(editor instanceof Ext.grid.CellEditor)) {
                if (!(editor instanceof Ext.form.field.Base)) {
                    editor = Ext.ComponentManager.create(editor, 'textfield');
                }
                editor = me.setConfig(propName, 'editor', new Ext.grid.CellEditor({ field: editor }));
            }
        } else if (type) {
            switch (type) {
                case 'date':
                    editor = editors.date;
                    break;
                case 'number':
                    editor = editors.number;
                    break;
                case 'boolean':
                    editor = me.editors['boolean'];
                    break;
                default:
                    editor = editors.string;
            }
        } else if (Ext.isDate(val)) {
            editor = editors.date;
        } else if (Ext.isNumber(val)) {
            editor = editors.number;
        } else if (Ext.isBoolean(val)) {
            editor = editors['boolean'];
        } else {
            editor = editors.string;
        }

        // Give the editor a unique ID because the CellEditing plugin caches them
        editor.editorId = propName;
        editor.field.column = me.valueColumn;
        return editor;
    },

    // Custom implementation of walkCells which only goes up and down.
    // Runs in the scope of the TableView
    walkCells: function(pos, direction, e, preventWrap, verifierFn, scope) {
        var valueColumn = this.ownerCt.valueColumn;

        if (direction == 'left') {
            direction = 'up';
        } else if (direction == 'right') {
            direction = 'down';
        }
        pos = Ext.view.Table.prototype.walkCells.call(this, pos, direction, e, preventWrap, verifierFn, scope);

        // We are only allowed to navigate to the value column.
        pos.column = valueColumn;
        pos.colIdx = valueColumn.getVisibleIndex();
        return pos;
    },

    setProperty: function(prop, value, create) {
        this.store.setValue(prop, value, create);
    },

    removeProperty: function(prop) {
        this.store.remove(prop);
    },

    // @private
    onUpdate : function(store, record, operation) {
        var me = this,
            value, oldValue;
        if (me.rendered && operation == Ext.data.Model.EDIT) {
            value = record.get(me.valueField);
            oldValue = record.modified.value;
            if (me.fireEvent('beforepropertychange', me.source, record.getId(), value, oldValue) !== false) {
                if (me.source) {
                    // me.source[record.getId()] = value;
                    Ext.Array.findBy(me.source, function(item) {return item.name == record.data.name;}).value = value;
                }
                record.commit();
                me.fireEvent('propertychange', me.source, record.getId(), value, oldValue);
            } else {
                record.reject();
            }
        }
    },

    beforeDestroy: function() {
        var me = this;
        me.callParent();
        me.destroyEditors(me.editors);
        me.destroyEditors(me.customEditors);
        delete me.source;
    },

    destroyEditors: function (editors) {
        for (var ed in editors) {
            if (editors.hasOwnProperty(ed)) {
                Ext.destroy(editors[ed]);
            }
        }
    }

})
