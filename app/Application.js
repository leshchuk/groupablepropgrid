/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('GPG.Application', {
    extend: 'Ext.app.Application',

    name: 'GPG',

    launch: function () {
        window.gpg = Ext.create("GPG.view.property.Grid", {
            source: {
                name: "test",
                start: new Date(),
                modified: new Date(),
                width: 100,
                height: 200,
                state: true,
                nothing: null
            },
            sourceConfig: {
                width: {
                    group: "Dimensions"
                },
                height: {
                    group: "Dimensions"
                }
            },
            groupsConfig: {
                dates: ["start", "modified"],
                // dimentions: ["width", "height"]
            },
            collapsibleGroups: true,
            // sortableColumns: false,
            renderTo: Ext.getBody(),
            width: 300
        });
    }

});
